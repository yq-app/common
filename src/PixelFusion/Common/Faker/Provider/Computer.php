<?php

namespace PixelFusion\Common\Faker\Provider;

class Computer extends \Faker\Provider\Base
{

    /**
     * An array of valid MAC address characters
     * @var array
     */
    private static $mac_address_vals = array(
        "0", "1", "2", "3", "4", "5", "6", "7",
        "8", "9", "A", "B", "C", "D", "E", "F"
     );

    /**
     * Generate a MAC address
     *
     * @param  string $separator - the section separator
     * @return string
     */
    public function macAddress($separator = ':')
    {
        $vals = self::$mac_address_vals;
        if (count($vals) >= 1) {
            $mac = array("00"); // set first two digits manually
            while (count($mac) < 6) {
                shuffle($vals);
                $mac[] = $vals[0] . $vals[1];
            }
            $mac = implode($separator, $mac);
        }
        return $mac;
    }

    /**
     * Generate a private network ip address
     *
     * @return string
     */
    public function localIp()
    {
        $start = ['10', '192'];

        $ip = $start[rand(0, 1)];

        if ($ip === '192') {
            $ip .= '.168';
        } else {
            $ip .= '.' . rand(1, 255);
        }

        $ip .= sprintf('.%s.%s', rand(1, 255), rand(1, 255));

        return $ip;
    }

}
