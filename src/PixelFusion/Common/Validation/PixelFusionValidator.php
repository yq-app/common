<?php
namespace PixelFusion\Common\Validation;

/**
 * Class PixelFusionValidator
 *
 * @package PixelFusion
 * @subpackage Common\Validation
 * @author Pixel Fusion <info@pixelfusion.co.nz>
 */
class PixelFusionValidator extends \Illuminate\Validation\Validator
{
    
    /**
     * Validate the boolean value.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     *
     * @return bool
     */
    public function validateBoolean($attribute, $value, $parameters)
    {
        return is_bool($value);
    }

    /**
     * Validate the value is in the array provided (case insensitive)
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     *
     * @return void
     */
    public function validateSimpleIn($attribute, $value, $parameters)
    {
        return parent::validateIn($attribute, strtolower($value), array_map('strtolower', $parameters));
    }
    
}

