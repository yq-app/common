<?php

namespace PixelFusion\Common\Validation;

use Illuminate\Support\ServiceProvider;
use Validator;

class PixelFusionValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        Validator::resolver(
            function ($translator, $data, $rules, $messages) {
                return new PixelFusionValidator($translator, $data, $rules, $messages);
            }
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // No need to register anything :)
    }
}
