<?php

if (function_exists('fileSizeFormat') === false)
{
    /**
     * Get the calculated size of a file
     *
     * @param  mixed $bytesOrPath   Either the bytes or file path
     * @param  string $pattern      Output pattern
     *
     * @return string
     */
    function fileSizeFormat($bytesOrPath, $pattern = '%d %s')
    {

        $bytes = $bytesOrPath;
        if (is_string($bytesOrPath) && file_exists($bytesOrPath))
        {
            $bytes = sprintf('%u', filesize($bytesOrPath));
        }

        if ($bytes > 0)
        {
            $unit = intval(log($bytes, 1024));
            $units = array('B', 'KB', 'MB', 'GB');

            if (array_key_exists($unit, $units) === true)
            {
                return sprintf($pattern, $bytes / pow(1024, $unit), $units[$unit]);
            }
        }

        return $bytes;
    }
}

if (function_exists('is_valid_mac') === false)
{
    /**
     * Check if a given MAC address is valid
     *
     * @param string $macAddress MAC address
     *
     * @return boolean
     */
    function is_valid_mac($macAddress)
    {
        $regex = '/([a-fA-F0-9]{2}[:|\-]?){6}/';

        return (bool) (preg_match($regex, $macAddress) == 1);
    }
}
