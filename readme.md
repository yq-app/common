# Pixel Fusion Common Package

The Pixel Fusion Common package includes some basic classes, helpers, templates to make some things a little easier. Feel free to add your own stuff but make sure you write tests and create a new tag after you've pushed your changes.


## Installation

By default the Pixel Fusion Common plugin should already be installed for your project. If not you can install it through Composer. Edit your project's `composer.json` file to require `pixelfusion/common`.

	"repositories": [{
        "type": "vcs",
        "url": "https://bitbucket.org/PixelFusion/common.git"
    }],
	"require": {
		"pixelfusion/common": "1.*"
	}

Next, update Composer from the Terminal:

    composer update


## Custom Validators

This package includes the PixelFusionValidator class for all your custom validation needs. To use it, just add the service provider to the providers array in your `app/config/app.php`:

    'PixelFusion\Common\Validation\PixelFusionValidatorServiceProvider'

### Available rules

* **boolean** - The field under validation must be either true or false
* **simple_in** - The value must be one of the parameters given, however is case insensitive

### Error messages

To make sure you output a nice error message when the validation fails you have to add the error message to the `app/lang/en/validation.php` file. For example to a user friendly message for the boolean validator you would add the following to the message array:

    'boolean' => 'The :attribute must be a boolean'

## Custom Generators

Included in the Pixel Fusion Common plugin are customised generator templates for Jeffrey Way's `way/generators` package. The templates are modified to be PSR-2 compliant and include basic phpDoc.

To use our custom templates you first have to publish the config of the generators package:

    php artisan config:publish way/generators

This will generate the following config file: `app/config/packages/way/generators/config.php`

Next, you have to replace the contents of this file with the following:

    <?php

    return [
        // Where the templates for the generators are stored...
        'model_template_path'               => 'vendor/pixelfusion/common/src/PixelFusion/Common/Generators/templates/model.txt',
        'scaffold_model_template_path'      => 'vendor/pixelfusion/common/src/PixelFusion/Common/Generators/templates/scaffolding/model.txt',
        'controller_template_path'          => 'vendor/pixelfusion/common/src/PixelFusion/Common/Generators/templates/controller.txt',
        'scaffold_controller_template_path' => 'vendor/pixelfusion/common/src/PixelFusion/Common/Generators/templates/scaffolding/controller.txt',
        'migration_template_path'           => 'vendor/pixelfusion/common/src/PixelFusion/Common/Generators/templates/migration.txt',
        'seed_template_path'                => 'vendor/pixelfusion/common/src/PixelFusion/Common/Generators/templates/seed.txt',
        'view_template_path'                => 'vendor/pixelfusion/common/src/PixelFusion/Common/Generators/templates/view.txt',

        // Where the generated files will be saved...
        'model_target_path'      => app_path('models'),
        'controller_target_path' => app_path('controllers'),
        'migration_target_path'  => app_path('database/migrations'),
        'seed_target_path'       => app_path('database/seeds'),
        'view_target_path'       => app_path('views')
    ];

Now whenever you use one of the `php artisan generate:...` commands it will use the custom template.
