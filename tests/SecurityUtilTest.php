<?php

use PixelFusion\Common\Security\SecurityUtil;

/**
 * @package pfStandardPlugin
 * @subpackage test
 */
class SecurityUtilTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests generateUuid()
     *
     */
    public function testGenerateUuid()
    {
        $result = SecurityUtil::generateUuid();

        // e.g. af61773a-6714-4315-be80-39a6a38f103f
        $this->assertEquals(36, strlen($result));
    }
}
