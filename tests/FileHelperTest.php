<?php

/**
 * @package pfStandardPlugin
 * @subpackage test
 */
class FileHelperTest extends PHPUnit_Framework_TestCase
{
    /**
     * Tests generateUuid()
     *
     */
    public function testFileSize()
    {
        $this->assertEquals('500 B', fileSizeFormat(500));
        $this->assertEquals('5 KB', fileSizeFormat(1024 * 5));
        $this->assertEquals('5 MB', fileSizeFormat(1024 * (5 * 1024)));
        $this->assertEquals('5 GB', fileSizeFormat(1024 * (5 * 1024) * 1024));
    }
}
